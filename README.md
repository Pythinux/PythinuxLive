# Pythinux Live
**NOTE: Pythinux Live is not currently usable.**
## About
Pythinux is an operating system written entirely in Python. Being written in python means that it has always needed an underlying operating system to run.

Pythinux Live contains a FULL bootable environment, meaning that you can run Pythinux on real hardware, with a fully working Linux kernel.
## How to run
1. Download an ISO image of a release. If you do not want to do that, you can build it from source.
2. Burn the ISO to a USB or DVD.
3. Boot into the newly burned DVD or USB.
4. Profit.

Pythinux Live is currently designed to run on virtual machines, and setting it up to run on your physical hardware may be a challange.

Currently, no installer for Pythinux Live exists, so you can only run off the bootable USB.
## Building
To build, you need Arch Linux or an Arch Linux-based distribution.
### Download the required dependencies
```
$ sudo pacman -S git base-devel archiso
```
### Clone the repository
```
$ git clone https://codeberg.org/WinFan3672/PythinuxLive
```
### `cd` into it
``` 
$ cd PythinuxLive
```
### make the build script executable
```
$ chmod +x build.sh
```
### Run the build script
```
$ sudo ./build.sh
```
## Forking
Forks of the project are welcome. However, do note the following:
* In `archiso/pacman.conf`, the repository is hard-coded to this repository. You'll need to change it.
* All PKGBUILDS point to this repository as a source, so change them before building or you'll build the original.

## Contributing
Feel free to create an issue or pull request.
## Support
If you have an issue related to Pythinux Live, feel free to make an Issue. However, if the issue is related to Arch Linux itself, then it isn't our problem. However, if you are unsure, we will still be willing to help.
